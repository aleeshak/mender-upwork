package com.app.handyman.mender.common.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.handyman.mender.R;
import com.app.handyman.mender.common.activity.EditProfileActivity;
import com.app.handyman.mender.model.Request;
import com.app.handyman.mender.user.adapter.RequestsAdapter;
import com.app.handyman.mender.user.fragment.PaymentMethods;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Fragment to manage user profile!
 */

public class ProfileFragment extends Fragment {

    private RecyclerView mRecList;
    private RequestsAdapter mAdapter;
    private ArrayList<Request> listOfRequests = new ArrayList<>();
    private Button paymentmethods;
    private TextView mName, mEmail, mPhoneNumber, mAddress, previousjobs, previousjobs2, mCityAndState;
    private ProgressBar progressBar;

    DatabaseReference mRootReference = FirebaseDatabase.getInstance().getReference();
    DatabaseReference requestsReference = mRootReference.child("Requests");

    private Button mEditProfile;

    private FirebaseAuth auth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        // Setup Widgets
        mRecList = (RecyclerView) v.findViewById(R.id.my_requests_recyclerview);

        mName = (TextView) v.findViewById(R.id.name);
        Typeface myCustomFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand Book.otf");
        mName.setTypeface(myCustomFont);


        mEmail = (TextView) v.findViewById(R.id.email);
        Typeface myCustomFont1 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand Book.otf");
        mEmail.setTypeface(myCustomFont1);

        mPhoneNumber = (TextView) v.findViewById(R.id.phone_number);
        Typeface myCustomFont2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand Book.otf");
        mPhoneNumber.setTypeface(myCustomFont2);

        mAddress = (TextView) v.findViewById(R.id.address);
        Typeface myCustomFont3 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand Book.otf");
        mAddress.setTypeface(myCustomFont3);

        mCityAndState = (TextView) v.findViewById(R.id.city_and_state);
        Typeface myCustomFont4 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand Book.otf");
        mCityAndState.setTypeface(myCustomFont4);

        mEditProfile = (Button) v.findViewById(R.id.edit_profile);
        Typeface myCustomFont5 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand Book.otf");
        mEditProfile.setTypeface(myCustomFont5);

        paymentmethods = (Button) v.findViewById(R.id.paymentmethods);
        Typeface myCustomFont7 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand Book.otf");
        paymentmethods.setTypeface(myCustomFont7);

        previousjobs = (TextView) v.findViewById(R.id.previousjobs);
        Typeface myCustomFont6 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand Book.otf");
        previousjobs.setTypeface(myCustomFont6);

        previousjobs2 = (TextView) v.findViewById(R.id.previousjobs2);
        previousjobs2.setTypeface(myCustomFont6);

        progressBar = (ProgressBar)v.findViewById(R.id.progressBar);

        paymentmethods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), PaymentMethods.class);
                startActivity(intent);

            }
        });

        mEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                startActivity(intent);

            }
        });

        getUserDetails();

        // Getting Users Jobs List.
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        mRecList.setLayoutManager(llm);

        listOfRequests = new ArrayList<>();

        mAdapter = new RequestsAdapter(getActivity(), listOfRequests);
        mRecList.setAdapter(mAdapter);




        return v;
    }


    private void getJobRequests(){
        FirebaseUser u = FirebaseAuth.getInstance().getCurrentUser();
        if (u != null) {
            Query usersRequestReference = requestsReference.orderByChild("userEmail").equalTo(FirebaseAuth.getInstance().getCurrentUser().getEmail());

            usersRequestReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    listOfRequests.clear();
                    mAdapter.notifyDataSetChanged();

                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Request f = data.getValue(Request.class);
                        listOfRequests.add(f);
                        mAdapter.notifyDataSetChanged();
                    }

                    if (listOfRequests.size() == 0) {
                        previousjobs2.setVisibility(View.VISIBLE);
                    } else {
                        previousjobs2.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Toast.makeText(getContext(), "Cancelled Request", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    private void getUserDetails() {

        auth = FirebaseAuth.getInstance();
        FirebaseUser u = FirebaseAuth.getInstance().getCurrentUser();
        if (u != null) {
            getJobRequests();

            final String email = auth.getCurrentUser().getEmail();
            DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
            DatabaseReference gameRef = mRootRef.child("Users");
            Query queryRef = gameRef.orderByChild("userEmail").equalTo(email);
            queryRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot snapshot, String previousChild) {
                    Iterable<DataSnapshot> children = snapshot.getChildren();

                    progressBar.setVisibility(View.GONE);

                    Iterator<DataSnapshot> child = children.iterator();

                    while (child.hasNext()) {



                        DataSnapshot currentChild = child.next();
                        if (currentChild.getKey().equals("firstName")) {
                            mName.setText("" + currentChild.getValue());
                        } else if (currentChild.getKey().equals("lastName")) {
                            String firstName = mName.getText().toString();
                            mName.setText(firstName + " " + currentChild.getValue());
                        } else if (currentChild.getKey().equals("address")) {
                            mAddress.setText("" + currentChild.getValue());
                        } else if (currentChild.getKey().equals("cityState")) {
                            mCityAndState.setText("" + currentChild.getValue());
                        } else if (currentChild.getKey().equals("userEmail")) {
                            mEmail.setText("" + currentChild.getValue());
                        } else if (currentChild.getKey().equals("phonenumber")) {
                            mPhoneNumber.setText("" + currentChild.getValue());
                        } else {

                        }


                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }


    }

}
